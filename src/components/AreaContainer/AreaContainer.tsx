import * as React from 'react';
import GameArea from './GameArea/GameArea';
import store from '../../redux';
import { Provider } from 'react-redux';

export class AreaContainer extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <GameArea />
      </Provider>
    );
  }
}