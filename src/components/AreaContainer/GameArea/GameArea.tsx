import * as React from 'react';
import './GameArea.css';
import StatusBar from './StatusBar/StatusBar';
import Grid from './Grid/Grid';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IRootState } from '../../../redux/models/rootState';
import { generateField } from '../../../redux/actions/field';

interface Props {
  generate: typeof generateField;
}

class GameArea extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
    props.generate();
  }
  render() {
    return (
      <div className="area-wrapper">
        <StatusBar />
        <Grid/>
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: Dispatch<IRootState>) => bindActionCreators({
  generate: generateField
},                                                                                dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GameArea);