import * as React from 'react';
import './Score.css';
import { IRootState } from '../../../../../redux/models/rootState';
import { connect } from 'react-redux';
import { IShipType } from '../../../../../redux/models/shipConfig';
import { IShipState } from '../../../../../redux/models/ship';
import { values, sumBy, sum } from 'lodash';

interface Props {
  shipTypes: {[key: string]: IShipType};
  ships: IShipState;
}

class Score extends React.PureComponent<Props> {
  getScore(): string {
    const countSurvivedCells = sumBy(values(this.props.ships), 'countSurvivedCells');
    if (countSurvivedCells === 0) {
      return 'WIN!!!';
    }

    const allCells = sum(values(this.props.shipTypes).map((it: IShipType) => it.size * it.count));
    const score = allCells - countSurvivedCells;
    return score < 10 ? `0${score}` : score + '';
  }

  render() {
    return (
      <div className="score-panel">
        <div className="score">
          <div className="head">{this.getScore()}</div>
          <div className="divider"/>
          <div className="footer">player 1</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  shipTypes: state.shipConfig.shipTypes,
  ships: state.ships
});

export default connect(mapStateToProps)(Score);