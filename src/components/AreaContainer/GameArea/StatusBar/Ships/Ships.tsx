import * as React from 'react';
import './Ships.css';
import Ship from './Ship/Ship';
import { IRootState } from '../../../../../redux/models/rootState';
import { connect } from 'react-redux';
import { IShipConfig } from '../../../../../redux/models/shipConfig';
import { IShip, IShipState } from '../../../../../redux/models/ship';
import { values } from 'lodash';

interface Props {
  shipConfig: IShipConfig;
  ships: IShipState;
}

class Ships extends React.PureComponent<Props> {
  render() {
    console.log('Ships!: ', this.props.ships);
    return (
      <div className="ships">
        {values(this.props.ships).filter(it => it).map((ship: IShip) => {
          const amount = this.props.shipConfig.shipTypes[ship.type].size;
          return (
            <Ship
              key={ship.id}
              type={ship.type}
              size={amount}
              killed={amount - ship.countSurvivedCells}
            />
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  shipConfig: state.shipConfig,
  ships: state.ships
});

export default connect(mapStateToProps)(Ships);