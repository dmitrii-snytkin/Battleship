import * as React from 'react';
import './Ship.css';
import { times } from 'lodash';

interface Props {
  type: string;
  size: number;
  killed: number;
}

export default class Ship extends React.PureComponent<Props> {
  renderScore() {
    return times(this.props.size).map((it, index) => {
      return <div key={index} className={index < this.props.killed ? 'hit' : 'miss'}/>;
    });
  }

  render() {
    return (
        <div className="ship-wrapper">
            <div className={this.props.type + ' ship'}/>
            {this.renderScore()}
        </div>
    );
  }
}