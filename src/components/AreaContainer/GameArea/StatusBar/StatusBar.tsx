import * as React from 'react';
import Score from './Score/Score';
import Ships from './Ships/Ships';
import './StatusBar.css';

export default class StatusBar extends React.PureComponent {
  render() {
    return (
      <div className="status">
        <Score/>
        <Ships/>
      </div>
    );
  }
}