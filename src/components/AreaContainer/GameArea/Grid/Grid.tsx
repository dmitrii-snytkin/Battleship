import * as React from 'react';
import './Grid.css';
import { IRootState } from '../../../../redux/models/rootState';
import { connect } from 'react-redux';
import { ICell } from '../../../../redux/models/cell';
import { selectCell } from '../../../../redux/actions/field';
import { bindActionCreators, Dispatch } from 'redux';
import { IShip, IShipState } from '../../../../redux/models/ship';

interface Props {
  field: ICell[][];
  selectCell: typeof selectCell;
  ships: IShipState;
}

class Grid extends React.PureComponent<Props> {
  getClassesByCell(cell: ICell): string {
    let classes = 'cell ';
    if (cell.visited) {
      const shipId = cell.shipId;
      if (shipId) {
        const ship: IShip = this.props.ships[shipId];
        if (ship.countSurvivedCells === 0) {
          classes += 'died';
        } else {
          classes += 'hit';
        }
      } else {
        classes += 'miss';
      }
    }
    return classes;
  }

  generateGrid() {
    let index = 0;
    return this.props.field.map((row, y) => {
      return row.map((cell: ICell, x) => {
        return (
          <div key={++index} className={this.getClassesByCell(cell)} onClick={() => this.props.selectCell({x, y})}/>
        );
      });
    });
  }

  render() {
    return (
      <div className="grid">
        {this.generateGrid()}
      </div>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  field: state.field.field,
  ships: state.ships
});

const mapDispatchToProps = (dispatch: Dispatch<IRootState>) => bindActionCreators({
  selectCell
},                                                                                dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Grid);