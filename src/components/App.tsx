import * as React from 'react';
import './App.css';
import { AreaContainer } from './AreaContainer/AreaContainer';

class App extends React.PureComponent {
  render() {
    return (
      <div className="App">
        <AreaContainer />
      </div>
    );
  }
}

export default App;
