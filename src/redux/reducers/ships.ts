import { IShipState } from '../models/ship';
import { ShipsAction, shipsActions } from '../actions/ships';
import { getType } from '../../vendor/typesafe-actions/src';

export const ships = (state: IShipState = {}, action: ShipsAction) => {
  switch (action.type) {
    case getType(shipsActions.resetShips): {
      return action.payload;
    }
    case getType(shipsActions.updateShip): {
      return {...state, [action.payload.id]: action.payload};
    }
    default: return state;
  }
};