import { fieldActions, FieldActions } from '../actions/field';
import { GameState, IField } from '../models/field';
import { getType } from '../../vendor/typesafe-actions/src';

const initialState: IField = {
  gameState: GameState.Initial,
  field: []
};

export const field = (state: IField = initialState, action: FieldActions): IField => {
  switch (action.type) {
    case getType(fieldActions.setField): {
      return {...state, field: action.payload};
    }
    case getType(fieldActions.setGameState): {
      return {...state, gameState: action.payload};
    }
    case getType(fieldActions.updateCell): {
      const newRow = state.field[action.payload.y].map((cell, index) => {
        return index === action.payload.x ? action.payload.cell : cell;
      });
      const newField = state.field.map((row, index) => {
        return index === action.payload.y ? newRow : row;
      });
      return {...state, field: newField};
    }
    default: return state;
  }
};