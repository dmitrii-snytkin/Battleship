import { ICell } from '../models/cell';
import { canPutShip, fill } from './generateField';
import { cloneDeep } from 'lodash';

describe('generateField', function () {
  let field: ICell[][] = Array(10)
    .fill([])
    .map(() => {
      return Array(10).fill(null).map(() => ({
        visited: false
      }));
    });
  let expectedResult = cloneDeep(field);
  describe('#fill', () => {
    let shipId: number;
    beforeEach(() => {
      shipId = 1;
    });
    it('should put horizontal ship correctly', () => {
      fill(3, 2, 5, true, field, shipId);
      for (let i = 3; i < 8; ++i) {
        expectedResult[2][i] = {
          visited: false,
          shipId
        };
      }
      for (let i = 0; i < 10; ++i) {
        expect(expectedResult[i]).toEqual(field[i]);
      }
    });
    it('should put vertical ship correctly', () => {
      fill(3, 2, 5, false, field, shipId);
      for (let i = 2; i < 7; ++i) {
        expectedResult[i][3] = {
          visited: false,
          shipId
        };
      }

      for (let i = 0; i < 10; ++i) {
        expect(expectedResult[i]).toEqual(field[i]);
      }
    });
  });
  describe('#canPutShip', () => {
    beforeEach(() => {
      field[5][5] = {
        shipId: 1,
        visited: true
      };
    });
    describe('horizontal ship', () => {
      it('should be put successful', () => {
        expect(canPutShip(5, 7, 2, true, field)).toEqual(true);
      });
      it('shoudn\'t be put in filled ceil', () => {
        expect(canPutShip(3, 5, 5, true, field)).toEqual(false);
      });
      it('shoudn\'t be put close to other ships', () => {
        expect(canPutShip(0, 5, 5, true, field)).toEqual(false);
      });
    });
    describe('vertical ship', () => {
      it('should be put successful', () => {
        expect(canPutShip(5, 7, 2, false, field)).toEqual(true);
      });
      it('shoudn\'t be put in filled ceil', () => {
        expect(canPutShip(5, 3, 5, false, field)).toEqual(false);
      });
      it('shoudn\'t be put close to other ships', () => {
        expect(canPutShip(5, 0, 5, false, field)).toEqual(false);
      });
    });
  });
});