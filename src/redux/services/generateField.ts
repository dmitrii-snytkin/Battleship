import { ICell } from '../models/cell';

export function fill(
  x: number,
  y: number,
  size: number,
  isHorizontal: boolean,
  field: ICell[][],
  shipId?: number
) {
  const makeCell = (): ICell => ({visited: false, shipId});
  if (isHorizontal) {
    for (let i = x; i < x + size; ++i) {
      field[y][i] = makeCell();
    }
  } else {
    for (let i = y; i < y + size; ++i) {
      field[i][x] = makeCell();
    }
  }
}

export function createEmptyField(width: number, height: number): ICell[][] {
  return Array(height)
    .fill([])
    .map(() => {
      return Array(width).fill(null).map(() => ({
        visited: false
      }));
    });
}

export function canPutShip(
  x: number,
  y: number,
  size: number,
  isHorizontal: boolean,
  field: ICell[][]
): boolean {
  const fromX = Math.max(0, x - 1);
  const toX = Math.min(field[0].length - 1, isHorizontal ? x + size : x + 1);
  const fromY = Math.max(0, y - 1);
  const toY = Math.min(field.length - 1, isHorizontal ? y + 1 : y + size);
  let isFilled = false;
  for (let i = fromY; i <= toY; ++i) {
    for (let j = fromX; j <= toX; ++j) {
      isFilled = isFilled || !!field[i][j].shipId;
    }
  }

  return !isFilled;
}