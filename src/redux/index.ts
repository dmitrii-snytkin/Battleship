import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { field } from './reducers/field';
import { ships } from './reducers/ships';
import thunk from 'redux-thunk';
import { shipConfig } from './reducers/shipConfig';

export const rootReducer = combineReducers({
  field,
  ships,
  shipConfig
});

function configureStore() {
  const enhancer = compose(
    applyMiddleware(thunk)
  );
  return createStore(
    rootReducer,
    enhancer
  );
}

const store = configureStore();

export default store;