export interface IShipState {
  [key: number]: IShip;
}

export interface IShip {
  readonly id: number;
  readonly type: string;
  readonly countSurvivedCells: number;
}
