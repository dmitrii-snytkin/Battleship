export interface ICell {
  readonly visited: boolean;
  readonly shipId?: number;
}