import { ICell } from './cell';

export enum GameState {
  Initial = 0,
  Started = 1,
  Win = 2
}

export interface IField {
  readonly gameState: GameState;
  field: ReadonlyArray<ReadonlyArray<ICell>>;
}