import { IField } from './field';
import { IShipState } from './ship';
import { IShipConfig } from './shipConfig';

export interface IRootState {
  field: IField;
  ships: IShipState;
  shipConfig: IShipConfig;
}