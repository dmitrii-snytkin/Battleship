export interface IShipType {
  readonly size: number;
  readonly count: number;
}

export interface IShipConfig {
  readonly shipTypes: {[key: string]: IShipType};
  readonly width: number;
  readonly height: number;
}