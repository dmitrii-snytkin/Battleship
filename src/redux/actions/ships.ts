import { IShip } from '../models/ship';
import { createAction } from '../../vendor/typesafe-actions/src';
import { $call } from 'utility-types';

const RESET_SHIPS = 'RESET_SHIPS';
const UPDATE_SHIP = 'UPDATE_SHIP';

export const shipsActions = {
  resetShips: createAction(RESET_SHIPS, (payload: {[key: number]: IShip}) => ({
    type: RESET_SHIPS,
    payload
  })),
  updateShip: createAction(UPDATE_SHIP, (payload: IShip) => ({
    type: UPDATE_SHIP,
    payload
  }))
};

const returnsOfActions = Object.values(shipsActions).map($call);
export type ShipsAction = typeof returnsOfActions[number];