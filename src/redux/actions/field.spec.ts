import { ICell } from '../models/cell';
import { IRootState } from '../models/rootState';
import { selectCell } from './field';
import { cloneDeep } from 'lodash';
import { GameState } from '../models/field';

describe('field', () => {
  let field: ICell[][];
  let state: IRootState;
  let actions: any[];
  let getState = () => state;
  let dispatch = (data: any) => actions.push(data);
  const updateCellAction = {
    type: 'UPDATE_CELL',
    payload: {
      x: 3,
      y: 2,
      cell: {
        visited: true,
        shipId: undefined
      }
    }
  };
  beforeEach(() => {
    field = Array(10)
      .fill([])
      .map(() => {
        return Array(10).fill(null).map(() => ({
          visited: false
        }));
      });

    state = {
      shipConfig: {
        shipTypes: {},
        height: 10,
        width: 10
      },
      ships: {
        1: {
          type: '1', countSurvivedCells: 1, id: 1
        },
        2: {
          type: '2', countSurvivedCells: 2, id: 2
        }
      },
      field: {
        gameState: GameState.Started,
        field
      }
    };

    actions = [];
  });
  describe('selectCell', () => {
    it('should do nothing, if game finished', () => {
      (state.field.gameState as any) = GameState.Win;
      const createdAction = selectCell({
        x: 0, y: 0
      });
      createdAction(dispatch, getState, undefined);
      expect(actions).toEqual([]);
    });
    it('should do nothing, if cell is visited', () => {
      field[2][3] = {
        visited: true
      };
      const createdAction = selectCell({
        x: 3, y: 2
      });
      createdAction(dispatch, getState, undefined);
      expect(actions).toEqual([]);
    });

    it('should only mark cell as visited, if cell doesn\'t belong to any ship', () => {
      const createdAction = selectCell({
        x: 3, y: 2
      });
      createdAction(dispatch, getState, undefined);
      expect(actions).toEqual([updateCellAction]);
    });

    it('should only mark cell as visited, if shipId is invalid', () => {
      field[2][3] = {
        visited: false,
        shipId: 10
      };
      const createdAction = selectCell({
        x: 3, y: 2
      });
      const newUpdateAction = cloneDeep(updateCellAction);
      (newUpdateAction.payload.cell.shipId as any) = 10;
      createdAction(dispatch, getState, undefined);
      expect(actions).toEqual([newUpdateAction]);
    });

    it('should update ship', () => {
      field[2][3] = {
        visited: false,
        shipId: 2
      };
      const createdAction = selectCell({
        x: 3, y: 2
      });

      const newUpdateAction = cloneDeep(updateCellAction);
      (newUpdateAction.payload.cell.shipId as any) = 2;
      createdAction(dispatch, getState, undefined);
      expect(actions).toEqual([
        newUpdateAction,
        {
          type: 'UPDATE_SHIP',
          payload: {
            type: '2', countSurvivedCells: 1, id: 2
          }
        }
      ]);

      it('should set WIN game state, if all ships was killed', () => {
        state.ships = {
          1: {
            type: '1', countSurvivedCells: 0, id: 1
          },
          2: {
            type: '2', countSurvivedCells: 1, id: 2
          }
        };
        field[2][3] = {
          visited: false,
          shipId: 2
        };
        const createdAction = selectCell({
          x: 3, y: 2
        });
        createdAction(dispatch, getState, undefined);
        expect(actions).toEqual([
          newUpdateAction,
          {
            type: 'UPDATE_SHIP',
            payload: {
              type: '2', countSurvivedCells: 0, id: 2
            }
          },
          {
            type: 'SET_GAME_STATE',
            payload: GameState.Win
          }
        ]);
      });
    });
  });
});