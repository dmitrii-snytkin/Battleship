import { ICell } from '../models/cell';
import { shipsActions } from './ships';
import { IShipConfig, IShipType } from '../models/shipConfig';
import { canPutShip, createEmptyField, fill } from '../services/generateField';
import { times, toPairs, values, random } from 'lodash';
import { IShip } from '../models/ship';
import { generateId } from '../helpers/generateId';
import { GameState } from '../models/field';
import { IRootState } from '../models/rootState';
import { sumBy } from 'lodash';
import { createAction } from '../../vendor/typesafe-actions/src';
import { ThunkAction } from 'redux-thunk';
import { $call } from 'utility-types';

const SET_FIELD = 'SET_FIELD';
const SET_GAME_STATE = 'SET_GAME_STATE';
const UPDATE_CELL = 'UPDATE_CELL';

export interface ISelectCellParams {
  x: number;
  y: number;
}

export const fieldActions = {
  setField: createAction(SET_FIELD, (payload: ICell[][]) => ({
    type: SET_FIELD,
    payload
  })),
  setGameState: createAction(SET_GAME_STATE, (payload: GameState) => ({
    type: SET_GAME_STATE,
    payload
  })),
  updateCell: createAction(UPDATE_CELL, (payload: {x: number, y: number, cell: ICell}) => ({
    type: UPDATE_CELL,
    payload
  }))
};

export const generateField = (): ThunkAction<void, IRootState, void> => (dispatch, getState) => {
  const { width, height } = getState().shipConfig;
  const field: ICell[][] = createEmptyField(width, height);
  const shipConfig: IShipConfig = getState().shipConfig;
  const ships: {[key: number]: IShip} = [];
  toPairs(shipConfig.shipTypes).forEach(([shipName, item]: [string, IShipType]) => {
    times(item.count).forEach(() => {
      let created = false;
      while (!created) {
        const isHorizontal = random(0, 1) === 0;
        let x, y;
        if (isHorizontal) {
          x = random(0, field[0].length - item.size);
          y = random(0, field.length - 1);
        } else {
          x = random(0, field[0].length - 1);
          y = random(0, field.length - item.size);
        }
        if (canPutShip(x, y, item.size, isHorizontal, field)) {
          const ship: IShip = {
            countSurvivedCells: item.size,
            type: shipName,
            id: generateId()
          };
          ships[ship.id] = ship;
          fill(x, y, item.size, isHorizontal, field, ship.id);
          created = true;
        }
      }
    });
  });

  dispatch(fieldActions.setField(field));
  dispatch(shipsActions.resetShips(ships));
  dispatch(fieldActions.setGameState(GameState.Initial));
};

export const selectCell = (params: ISelectCellParams): ThunkAction<void, IRootState, void> => (dispatch, getState) => {
  if (getState().field.gameState === GameState.Win) {
    return;
  }

  const { field } = getState().field;
  const { x, y } = params;
  const cell: ICell = field[y][x];
  if (cell.visited) {
    return;
  }

  const newCell: ICell = {
    visited: true,
    shipId: cell.shipId
  };
  dispatch(fieldActions.updateCell({x, y, cell: newCell}));
  if (!cell.shipId) {
    return;
  }

  const ship: IShip = getState().ships[cell.shipId];
  if (!ship) {
    return ;
  }

  const survivedCells = sumBy(values(getState().ships), 'countSurvivedCells');
  const newShip: IShip = {...ship, countSurvivedCells: ship.countSurvivedCells - 1};
  dispatch(shipsActions.updateShip(newShip));
  if (survivedCells === 1) {
    dispatch(fieldActions.setGameState(GameState.Win));
  }
};

const returnsOfActions = Object.values(fieldActions).map($call);
export type FieldActions = typeof returnsOfActions[number];